import React from "react"
import Navibar from "../components/navigation/navibar"
import useTranslation from "next-translate/useTranslation"

function DefaultLayout(props) {
  const { t, lang } = useTranslation()

  return (
    <Navibar {...props} t={t} lang={lang}>
      {props.children}
    </Navibar>
  )
}

export default DefaultLayout
