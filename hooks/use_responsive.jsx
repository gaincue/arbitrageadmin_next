import { useState, useLayoutEffect } from "react"
import { useMediaQuery } from "react-responsive"

const breakpointMobile = 767
const breakpointTablet = 991
const breakpointDesktop = 996

function useResponsive() {
  const [isClient, setIsClient] = useState(false)

  const isMobile = useMediaQuery({
    maxWidth: breakpointMobile,
  })

  const isTablet = useMediaQuery({
    minWidth: breakpointMobile,
    maxWidth: breakpointTablet,
  })

  const isDesktop = useMediaQuery({
    minWidth: breakpointTablet,
  })

  const isWidescreen = useMediaQuery({
    minWidth: breakpointDesktop,
  })

  useLayoutEffect(() => {
    if (typeof window !== "undefined") setIsClient(true)
  }, [])

  return {
    isDesktop: isClient ? isDesktop : true,
    isTablet: isClient ? isTablet : false,
    isMobile: isClient ? isMobile : false,
    isWidescreen: isClient ? isWidescreen : false,
  }
}

export default useResponsive
