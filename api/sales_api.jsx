import { fetchGet } from "./fetch_api"
import moment from "moment"
import { isEmpty } from "lodash"
import axios from "axios";

export const getSales = async (params = {}) => {
  // console.log("getSales params:", params)

  try {
    const queryParams = params.queryParams
    const durationRange = params.range || [moment().add(-7, "days"), moment()]
    let argument = {
      ...queryParams,
      params: `from_date=${durationRange[0].format(
        "YYYY-MM-DD"
      )}&to_date=${durationRange[1].format(
        "YYYY-MM-DD"
      )}`,
    }
    const response = await fetchGet(`sales`, argument)

    let data = response.data.data.data
    let rangeUnit = response.data.data.range_unit
    let rangeValue = response.data.data.range_value
    const processedData = []

    data.map((value, key) => {
      processedData.push({
        x: value.x_label,
        y: value.total || 0,
      })
    })

    // console.log(processedData)
    return { data: processedData, unit: rangeUnit, value: rangeValue }
  } catch (error) {
    console.log(error)
  }
}

export const getTrades = async (params = {}) => {
  // console.log("getTrades params:", params)
  // console.log(params)

  try {
    const { queryParams, ...others } = params
    
    const response = await fetchGet("trades", {
      ...queryParams,
      ...others
    })

    return {
      data: response.data.data
    }
  } catch (error) {
    console.log(error)
  }
}


export const getSalesByExchange = async (params = {}) => {
  // console.log("getSalesByExchange params:", params)

  try {
    // const durationRange = params.range || [moment().add(-7, "days"), moment()]
    const exchangeDir = params.exchangeDir || "from_exchange"
    const response = await fetchGet(`sales-exchange`, {params:`field=${exchangeDir}`})
    let data = response.data.data
    const processedData = []
    data.data.map((value, key) => {
      processedData.push({
        x: value.exchange,
        y: value.total || 0,
      })
    })

    return { data, processedData }
  } catch (error) {
    console.log(error)
  }
}

export const getSalesByVending = async (params = {}) => {
  console.log("getSalesByVending params:", params)

  try {
    const durationRange = params.range || [moment().add(-7, "days"), moment()]

    const response = await fetchGet(`vending_sales`, {
      params: `from_date=${durationRange[0].format(
        "YYYY-MM-DD"
      )}&to_date=${durationRange[1].format("YYYY-MM-DD")}`,
    })

    let data = response.data.data

    return { data }
  } catch (error) {
    console.log(error)
  }
}

export const getSalesByUnit = async (params = {}) => {
  console.log("getSalesByUnit params:", params)

  try {
    const durationRange = params.range || [moment().add(-7, "days"), moment()]
    const sumField = "quantity"

    const response = await fetchGet(`product_sales`, {
      params: `sum_field=${sumField}&from_date=${durationRange[0].format(
        "YYYY-MM-DD"
      )}&to_date=${durationRange[1].format("YYYY-MM-DD")}`,
    })

    let data = response.data.data

    return { data }
  } catch (error) {
    console.log(error)
  }
}

export const getSalesByLocation = async (params = {}) => {
  console.log("getSalesByLocation params:", params)

  try {
    const durationRange = params.range || [moment().add(-7, "days"), moment()]

    const response = await fetchGet(`location_sales`, {
      params: `from_date=${durationRange[0].format(
        "YYYY-MM-DD"
      )}&to_date=${durationRange[1].format("YYYY-MM-DD")}`,
    })

    let data = response.data.data

    return { data }
  } catch (error) {
    console.log(error)
  }
}

export const getSalesIncome = async (params = {}) => {
  console.log("getSalesIncome params:", params)

  try {
    const response = await fetchGet(`vending_sales_income`, {
      ...params.queryParams,
    })

    let data = response.data.data

    return { data }
  } catch (error) {
    console.log(error)
  }
}

export const getSalesByPaymentMethod = async (params = {}) => {
  console.log("getSalesByPaymentMethod params:", params)

  try {
    const durationRange = params.range || [moment().add(-7, "days"), moment()]

    const response = await fetchGet(`payment_sales`, {
      params: `from_date=${durationRange[0].format(
        "YYYY-MM-DD"
      )}&to_date=${durationRange[1].format("YYYY-MM-DD")}`,
    })

    let data = response.data.data

    return { data }
  } catch (error) {
    console.log(error)
  }
}
