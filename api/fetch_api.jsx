import { parseCookies } from "nookies";
import { isEmpty } from "lodash";
import axios from "axios";

axios.interceptors.request.use(
  function (request) {
    // console.log("auth header:" + request.headers["Authorization"]);
    request.headers["Authorization"] = getBearerToken(); // insert bearer token automatically
    // Do something before request is sent
    return request;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

/**
 * Base URL for all API requests. No trailing slash.
 */
// export const baseUrl = "http://127.0.0.1:7000/api/v1";
export const baseUrl = "https://arbi1.a4de4694.com/api/v1";
// This is the testing key, which always pass:
// this is my own key for testing(which will not pass validation in backend unless you have the secret key):
//export const recaptchaSiteKey = "6Leh4doSAAAAAEEhi9GhrojoU_3-AnSVR3EG0hnf";

/**
 * Function to get the auth token, userid and company id, used by various API.
 * Directly extract from cookie since it may update/refresh dynamically.
 */
export const getAuthData = () => {
  const { token, user } = parseCookies();
  let userObj = "";

  if (!isEmpty(user) && user != "undefined") {
    userObj = JSON.parse(user);
  }

  return { token, user: userObj };
};

export const fetchGet = async (
  path,
  {
    ignorePagination = false,
    current = 1,
    pageSize = 5,
    sorter = "id:asc",
    params,
    filters = null
  }
) => {
  let otherParams = "";
  let filterParams = "";
  let paginationParams = "foo=bar";

  // const role = getRole()

  if (filters != null && filters != undefined) {
    filterParams = `&${Object.keys(filters)[0]}=${
      filters[Object.keys(filters)[0]]
    }`;
  }

  if (params != undefined) {
    otherParams = params;
  }

  if (!ignorePagination) {
    paginationParams = `_page=${current}&_limit=${pageSize}&_sort=${sorter}`;
  }

  // console.log(
  //   `${baseUrl}/${path}?${paginationParams}` +
  //     `${filterParams}&${otherParams}`
  // );

  try {
    const response = await axios.get(
      `${baseUrl}/${path}?${paginationParams}` +
        `${filterParams}&${otherParams}`
    );

    // const response = await fetch(
    //   `${baseUrl}/${path}?_page=${current}&_limit=${pageSize}&_sort=${sorter}` +
    //   `${teamIdParam}${companyIdParam}&${otherParams}`,
    //   {
    //     method: "GET",
    //     headers: {
    //       "Content-Type": "application/json",
    //       Authorization: `Bearer ${token.token}`
    //     }
    //   }
    // )

    // console.log("fetch get:", response.data);

    // if (response.status == 401) {
    //   throw new UnauthorizedError("Token Expired")
    // }

    return response;
  } catch (error) {
    throw error;
  }
};

/**
 * Return the string required for Authorization header for all APIs.
 */
export const getBearerToken = () => {
  const { token } = getAuthData();
  return `Bearer ${token}`;
};
