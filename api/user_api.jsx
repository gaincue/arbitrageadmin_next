import { login } from "../utils/auth";
import { baseUrl } from "./fetch_api";
import { isEmpty } from "lodash";
import axios from "axios";
import { openNotification } from "../utils/notifications";
/**
 * API to login existing user which is already activated.
 * @param {*} username
 * @param {*} password
 */
const loginUser = async (username, password) => {
  try {
    console.log('login user', username, password)
    const res = await axios.post(`${baseUrl}/auth/login`, {
      username,
      password,
    });
    console.log(res)
    // if status is not 2xx, then it will not reach this if-code
    if (res.data.success) {
      // axios provide the payload(http body) in .data
      const token = res.data.data.jwt.token;

      //ALICE
      const user = res.data.data.user;
      console.log(res.data.data);
      // openNotification(
      //   "USER_API  BEFORE login(token, user) token",
      //   token,
      //   "OK"
      // );
      // openNotification("USER_API BEFORE login(token, user) user", user, "OK");

      login(token, user);
    } else {
      throw new Error("Login failed");
    }

    return { success: true, error: "", message: "" };
  } catch (e) {
    if (e.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      console.log(e.response)
      return {
        success: false,
        error: "Request Error " + e.response.status + e.response.data.message,
      };
    } else if (e.request) {
      // The request was made but no response was received (timeout or network down)
      return { success: false, error: "Network Error " + e.message };
    } else {
      // Something happened in setting up the request that triggered an Error
      return { success: false, error: e.message };
    }
  }

  return { success: true, error: "" };
};

/**
 * API to register new user.
 * @param {*} firstName
 * @param {*} lastName
 * @param {*} password
 * @param {*} email
 */
const registerUser = async (
  firstName,
  lastName,
  password,
  email,
  recaptchaToken
) => {
  //console.log('register user')
  try {
    const res = await axios.post(`${baseUrl}/user/register`, {
      first_name: firstName,
      last_name: lastName,
      password: password,
      email: email,
      recaptcha_token: recaptchaToken,
    });
    return { success: true, message: "" };
  } catch (e) {
    if (e.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      return {
        success: false,
        error: "Request Error " + e.response.status + e.response.data.message,
      };
    } else if (e.request) {
      // The request was made but no response was received (timeout or network down)
      return { success: false, error: "Network Error " + e.message };
    } else {
      // Something happened in setting up the request that triggered an Error
      return { success: false, error: e.message };
    }
  }
};

/**
 * API to confirm new users.
 * @param {*} token The authentication token.
 * @returns the full json body of the response (contains success, data etc fields)
 */
const confirmUser = async (confirmToken) => {
  try {
    const res = await axios.get(`${baseUrl}/user/confirm/${confirmToken}`);
    return { success: true };
  } catch (e) {
    if (e.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      return {
        success: false,
        error: "Error:  " + e.response.status + e.response.data.message,
      };
    } else if (e.request) {
      // The request was made but no response was received (timeout or network down)
      return { success: false, error: "Error: " + e.message };
    } else {
      // Something happened in setting up the request that triggered an Error
      return { success: false, error: e.message };
    }
  }
};

/**
 * Update one user
 * @param {*} details Object containing the id and (updated)name of the user.
 */
const updateUser = async (details) => {
  try {
    // TODO: need to check ID as integer?
    const res = await axios.put(baseUrl + "/users/" + details.id, details);
    //console.log(res.data);

    return { success: true, error: "", result: res.data };
  } catch (e) {
    if (e.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      return { success: false, error: "Error:  " + e.response.data.message };
    } else if (e.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      return { success: false, error: "Error:  " + e.message };
    } else {
      // Something happened in setting up the request that triggered an Error
      return { success: false, error: e.message };
    }
  }
};

export { loginUser, registerUser, confirmUser, updateUser };
