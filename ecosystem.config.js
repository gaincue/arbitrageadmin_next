module.exports = {
  apps : [
      {
        name: "arbitadmin_next",
        script: "yarn start",
        watch: true,
        env: {
            "NODE_ENV": "production",
        }
      }
  ]
}     