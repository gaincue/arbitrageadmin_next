import React, { useState, useRef, useContext, useEffect } from "react"
import { Table, Form, Input, InputNumber, Button, Popconfirm } from "antd"
import styles from "./editable.less"
import useOuterClick from "../../hooks/use_outer_click"

const EditableContext = React.createContext()

export const EditableRow = ({ index, ...props }) => {
  const { editing, handleSave, children } = props
  const wrapperRef = useRef(null)
  useOuterClick(wrapperRef, () => {
    if (editing) {
      handleSave()
    }
  })

  return <tr ref={wrapperRef} {...props} />
}

export const EditableCell = ({
  editable,
  editing,
  dataIndex,
  title,
  inputType,
  record,
  index,
  children,
  edit,
  save,
  ...restProps
}) => {
  // const inputNode = inputType === "number" ? <InputNumber /> : <Input />

  return (
    <td {...restProps}>
      {editing ? (
        <Form.Item
          name={dataIndex}
          style={{
            margin: 0
          }}
        >
          <Input />
          {/* {inputNode} */}
          {/* ref={inputRef} */}
          {/* onPressEnter={save} onBlur={save} */}
        </Form.Item>
      ) : (
        <div
          className={editable ? "editable-cell-value-wrap" : ""}
          style={{
            paddingRight: 24
          }}
          // onMouseEnter={toggleEdit}
          onClick={() => {
            if (editable && !editing) {
              edit(record)
            }
          }}
        >
          &nbsp;
          {children}
        </div>
      )}
    </td>
  )
}
