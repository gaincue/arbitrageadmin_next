import React, { useRef } from "react"
import { PlusOutlined } from "@ant-design/icons"
import { Form, Button, message } from "antd"
import ProTable, { IntlProvider, enUSIntl } from "@ant-design/pro-table"
import { EditableCell, EditableRow } from "./editable"

const EditableTable = props => {
  const {
    handleSave,
    columns,
    cancel,
    data,
    pushSaves,
    newRow,
    isEditing,
    loading,
    form
  } = props
  const actionRef = useRef()

  return (
    <Form form={form} component={false}>
      <IntlProvider value={enUSIntl}>
        <ProTable
          actionRef={actionRef}
          rowKey="id"
          components={{
            body: {
              row: EditableRow,
              cell: EditableCell
            }
          }}
          onRow={(record, index) => {
            return {
              editing: isEditing(record),
              handleSave: () => handleSave(record)
            }
          }}
          columns={columns}
          rowClassName="editable-row"
          pagination={{
            onChange: cancel
          }}
          search={false}
          toolBarRender={(action, { selectedRows }) => [
            newRow != null && <>
              <Button type="primary" onClick={() => newRow()}>
                <PlusOutlined />
              </Button>
            </>

            // selectedRows && selectedRows.length > 0 && (
            //   <Dropdown
            //     overlay={
            //       <Menu
            //         onClick={async e => {
            //           if (e.key === "remove") {
            //             await handleRemove(selectedRows)
            //             action.reload()
            //           }
            //         }}
            //         selectedKeys={[]}
            //       >
            //         <Menu.Item key="remove">Bulk delete</Menu.Item>
            //         <Menu.Item key="approval">Bulk approve</Menu.Item>
            //       </Menu>
            //     }
            //   >
            //     <Button>
            //       Bulk action <DownOutlined />
            //     </Button>
            //   </Dropdown>
            // )
          ]}
          //   onChange={(_, _filter, _sorter) => {
          //     const sorterResult = _sorter

          //     if (sorterResult.field) {
          //       setSorter(`${sorterResult.field}_${sorterResult.order}`)
          //     }
          //   }}
          loading={loading}
          dataSource={data}
          bordered
        />
      </IntlProvider>
    </Form>
  )
}

export default EditableTable
