import React, { useState, useRef, useEffect } from "react"
import { useSelector } from "react-redux"
import { DownOutlined, PlusOutlined } from "@ant-design/icons"
import { Button, Dropdown, Menu } from "antd"
import ProTable, { IntlProvider, enUSIntl } from "@ant-design/pro-table"
import { keys, isEmpty } from "lodash"
import useTranslation from "next-translate/useTranslation"

import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import { handleModalVisible, setSelectedRow } from "store"

import UpdateForm from "../../components/forms/update_form"
import useResponsive from "../../hooks/use_responsive"

import "./dynamic_table.less"
const { parse } = require("json2csv")
import { CSVLink } from "react-csv"

const DynamicTable = props => {
  const {
    tableRef,
    title,
    getData,
    columns,
    newColumns,
    editColumns,
    hideToolBar,
    canSelect,
    childrenColumnName,
    handleAdd,
    handleEdit,
    onCancel,
    defaultSort="id:asc",
    scroll = { x: 1080, y: 400 },
    disablePagination = false, // used when needed to retrieve single object for table
    tabKey,
    currentTab
  } = props
  const { t } = useTranslation()
  const { isDesktop } = useResponsive()
  const { modalVisible, selectedRow } = props
  const { handleModalVisible, setSelectedRow } = props

  const [sorter, setSorter] = useState(defaultSort)
  const [total, setTotal] = useState(5)
  const [csv, setCsv] = useState([])
  const actionRef = tableRef != null ? tableRef : useRef()

  useEffect(() => {
    if (actionRef.current != null) {
      actionRef.current.reload()
    }
  }, [props.params]) // pass companyId to refresh table everytime selectedCompany is changed

  return (
    <>
      {columns && (
        <IntlProvider value={enUSIntl}>
          <ProTable
            rowKey="id"
            headerTitle={title}
            actionRef={actionRef}
            columns={columns}
            search={false}
            toolBarRender={(action, { selectedRows }) =>
              hideToolBar != null
                ? []
                : isDesktop
                ? [
                    <>
                      {newColumns && (
                        <Button
                          type="primary"
                          onClick={() => handleModalVisible(true)}
                        >
                          <PlusOutlined /> New {title}
                        </Button>
                      )}
                    </>,
                    <Button type='primary'>
                      <CSVLink
                        data={csv}
                        onClick={() => {
                          // console.log("You click the link") // 👍🏻 Your click handling logic
                        }}
                      >
                        {t("common:exportToCSV")}
                      </CSVLink>
                    </Button>,
                    selectedRows && selectedRows.length > 0 && (
                      <Dropdown
                        overlay={
                          <Menu
                            onClick={async e => {
                              if (e.key === "remove") {
                                await handleRemove(selectedRows)
                                action.reload()
                              }
                            }}
                            selectedKeys={[]}
                          >
                            <Menu.Item key="remove">Bulk delete</Menu.Item>
                            <Menu.Item key="approval">Bulk approve</Menu.Item>
                          </Menu>
                        }
                      >
                        <Button>
                          Bulk action <DownOutlined />
                        </Button>
                      </Dropdown>
                    ),
                  ]
                : [
                    <Button type="primary">
                      <CSVLink
                        data={csv}
                        onClick={() => {
                          // console.log("You click the link") // 👍🏻 Your click handling logic
                        }}
                      >
                        {t("common:exportToCSV")}
                      </CSVLink>
                    </Button>,
                  ]
            }
            {...(hideToolBar != null
              ? { options: false }
              : !isDesktop
              ? { options: false }
              : {})}
            onChange={(_pagination, _filter, _sorter) => {
              const sorterResult = _sorter
              let order

              if (sorterResult.order == "descend") {
                order = "desc"
              } else if (sorterResult.order == "ascend") {
                order = "asc"
              }

              if (order != undefined) {
                if (sorterResult.field) {
                  setSorter(`${sorterResult.field}:${order}`)
                }
              } else {
                setSorter("id:asc")
              }
            }}
            params={{
              sorter
            }}
            request={params => {
              if (props.params) {
                let keyList = keys(props.params)

                for (let key of keyList) {
                  if (key != "token") {
                    params[key] = props.params[key]
                  }
                }
              }

              if (!disablePagination) {
                params.queryParams = {
                  current: params.current,
                  pageSize: params.pageSize,
                  sorter: params.sorter
                }
              }

              return getData(params)
            }}
            postData={response => {
              // console.log("response dynamic")
              // console.log(response)
              if (response != null && !disablePagination) {
                setTotal(response.total)
              }
              response != null && response.length > 0 ? setCsv(parse(response)) : []
              
              return response
            }}
            onRequestError={error => {
              // console.log("error dynamic")
              // console.log(error)
            }}
            tableAlertRender={(selectedRowKeys, selectedRows) => {
              return !isEmpty(selectedRows) ? (
                <div>
                  Selected{" "}
                  <a
                    style={{
                      fontWeight: 600
                    }}
                  >
                    {selectedRowKeys.length}
                  </a>{" "}
                  row&nbsp;&nbsp;
                </div>
              ) : (
                false
              )
            }}
            bordered
            {...(canSelect ? { rowSelection: {} } : {})}
            pagination={
              !disablePagination
                ? {
                    pageSize: 50,
                    defaultCurrent: 1,
                    pageSizeOptions: ["10", "20", "50","100","200","500"],
                    showSizeChanger: true,
                    total: total
                  }
                : false
            }
            scroll={scroll}
            {...(childrenColumnName
              ? { childrenColumnName: childrenColumnName }
              : {})}
          />
        </IntlProvider>
      )}

      {(newColumns || editColumns) &&
        (tabKey != null ? (
          tabKey == currentTab && (
            <UpdateForm
              onCancel={
                onCancel
                  ? onCancel
                  : () => {
                      handleModalVisible(false)
                      setSelectedRow({})
                    }
              }
              modalVisible={modalVisible}
              title={isEmpty(selectedRow) ? `New ${title}` : `Edit ${title}`}
            >
              <IntlProvider value={enUSIntl}>
                <ProTable
                  onSubmit={async value => {
                    const success = isEmpty(selectedRow)
                      ? await handleAdd({ value })
                      : await handleEdit({ value })

                    if (success) {
                      handleModalVisible(false)
                      setSelectedRow({})

                      if (actionRef.current) {
                        actionRef.current.reload()
                      }
                    }
                  }}
                  rowKey="id"
                  type="form"
                  columns={
                    isEmpty(selectedRow)
                      ? newColumns || editColumns
                      : editColumns
                  }
                  rowSelection={{}}
                />
              </IntlProvider>
            </UpdateForm>
          )
        ) : (
          <UpdateForm
            onCancel={
              onCancel
                ? onCancel
                : () => {
                    handleModalVisible(false)
                    setSelectedRow({})
                  }
            }
            modalVisible={modalVisible}
            title={isEmpty(selectedRow) ? `New ${title}` : `Edit ${title}`}
          >
            <IntlProvider value={enUSIntl}>
              <ProTable
                onSubmit={async value => {
                  const success = isEmpty(selectedRow)
                    ? await handleAdd({ value })
                    : await handleEdit({ value })

                  if (success) {
                    handleModalVisible(false)
                    setSelectedRow({})

                    if (actionRef.current) {
                      actionRef.current.reload()
                    }
                  }
                }}
                rowKey="id"
                type="form"
                columns={
                  isEmpty(selectedRow) ? newColumns || editColumns : editColumns
                }
                rowSelection={{}}
              />
            </IntlProvider>
          </UpdateForm>
        ))}
    </>
  )
}

// const mapStateToProps = state => state
// const mapDispatchToProps = dispatch => {
//   return {
//     handleModalVisible: bindActionCreators(handleModalVisible, dispatch),
//     setSelectedRow: bindActionCreators(setSelectedRow, dispatch)
//   }
// }

// export default connect(mapStateToProps, mapDispatchToProps)(DynamicTable)
export default DynamicTable