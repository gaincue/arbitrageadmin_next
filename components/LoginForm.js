"use strict"
import { LockOutlined, UserOutlined } from "@ant-design/icons"
import { Layout, Row, Col, Button, Input, Form, message } from "antd"
import "@ant-design/compatible/assets/index.css"
import React, { useState, useEffect } from "react"
import Link from "next/link"
import { connect } from "react-redux"
import { setLoginState } from "store"
import useTranslation from "next-translate/useTranslation"

import { getAuthData } from "../api/fetch_api"
import { loginUser } from "../api/user_api"
import { openNotification } from "../utils/notifications"

/**
 * Template for LoginForm before wrapped with Form.Create
 * The form height is designed to fully fill the parent component's height automatically.
 * @param {} props The props to be received by this Component
 */
function LoginForm(props) {
  const { t } = useTranslation()
  const { onLoginSuccess } = props
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    if (props.expired) {
      openNotification("Expired", "Your session has expired", "OK")
    }
  }, [])

  // set the global loginState here, since all pages will require LoginForm
  // (or else put in top level components or Navibar)
  useEffect(() => {
    const { token } = getAuthData()
    if (token == null) {
      props.dispatch({ type: "SET_LOGIN_STATE", loginState: true })
    } else {
      props.dispatch({ type: "SET_LOGIN_STATE", loginState: true })
    }
  }, []) // we want to run only once to check login state

  const handleSubmit = async (values) => {
    setLoading(true)

    const username = values.username
    const password = values.password
    console.log("HandleSubmit login")
    const result = await loginUser(username, password)
    // console.log("HandleSubmit");
    console.log(result)
    if (!result.success) {
      // openNotification("Error", result.error, "OK");
      message.error("Your email or password is incorrect. Please try again.")
    } else {
      props.dispatch({ type: "SET_LOGIN_STATE", loginState: true })
      if (onLoginSuccess) {
        if (typeof onLoginSuccess === "function") {
          onLoginSuccess(result)
        } else {
          console.warn("onLoginSuccess is not a function!")
        }
      }
    }

    setLoading(false)
  }

  return (
    <>
      <Row gutter={0} style={{ height: "100%" }} type="flex" align="middle">
        <Col offset={8} span={8} style={{ textAlign: "center" }}>
          <img src="logo.svg" style={{ height: "100%", padding: "10px" }} />
        </Col>
      </Row>

      <Form onFinish={handleSubmit}>
        <Form.Item
          style={{ marginTop: 8, marginBottom: 8 }}
          name="username"
          rules={[
            { required: true, message: "Please enter a valid username." },
          ]}
        >
          <Input
            prefix={<UserOutlined style={{ color: "rgba(0,0,0,.25)" }} />}
            placeholder="Username"
          />
        </Form.Item>
        <Form.Item
          style={{ marginTop: 8, marginBottom: 8 }}
          name="password"
          rules={[{ required: true, message: "Please enter your password." }]}
        >
          <Input
            prefix={<LockOutlined style={{ color: "rgba(0,0,0,.25)" }} />}
            type="password"
            placeholder="Password"
          />
        </Form.Item>
        <Form.Item style={{ marginTop: 8, marginBottom: 8 }}>
          <Row>
            {/*Nesting another row/col inside*/}
            {/* <Col xs={24} lg={12} style={{ textAlign: "left" }}>
              <Link href="/forgot_password">
                <a className="login-form-forgot">
                  {t("common:forgotPassword")}
                </a>
              </Link>
            </Col> */}
            <Col xs={24} lg={24} style={{ textAlign: "right" }}>
              {/* <Button
                type="primary"
                href="/register"
                className="login-form-button"
                loading={loading}
                style={{ marginLeft: "5px", marginRight: "5px" }}
              >
                Sign up
              </Button> */}
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
                loading={loading}
                style={{ marginLeft: "5px", marginRight: "5px" }}
              >
                {t("common:login")}
              </Button>
            </Col>
          </Row>
        </Form.Item>
      </Form>
      {/* <footer className="footer_text" style={{ paddingTop: "30px" }}>
        By signing in, you agree to the <a href="#">terms of use</a> and{" "}
        <a href="#">privacy policy</a>.
      </footer> */}
    </>
  )
}

const mapStateToProps = (state) => {
  return { loginState: state.loginState }
}

export default connect(mapStateToProps)(LoginForm)
