import React from "react"
import { Button, Menu, Dropdown } from "antd"
import { useRouter } from "next/router"
import { logout } from "../../utils/auth"

import { UserOutlined, GlobalOutlined } from "@ant-design/icons"

const menuProfile = ({ t }) => {
  const router = useRouter()

  return (
    <Menu>
      <Menu.Item
        key="profLogOut"
        onClick={() => {
          logout()
          router.push("/")
        }}
      >
        <span>{t("common:logout")}</span>
      </Menu.Item>
    </Menu>
  )
}

const menuLanguage = ({ lang }) => {
  const router = useRouter()

  const changeLanguage = (lng) => {
    let ur = router.asPath.replace("#", "").replace(`/${lang}`, "")
    if (ur === "") {
      ur = "/"
    }

    router.push({ url: ur, options: { lang: lng } })
  }

  return (
    <Menu>
      <Menu.Item
        key="lang_cn"
        onClick={() => {
          changeLanguage("cn")
        }}
      >
        中文
      </Menu.Item>
      <Menu.Item
        key="lang_en"
        onClick={() => {
          changeLanguage("en")
        }}
      >
        EN
      </Menu.Item>
    </Menu>
  )
}

const NavHeader = ({ t, lang }) => {
  // console.log(t)
  // console.log(lang)
  return (
    <div className="header_icons">
      <Dropdown overlay={menuLanguage({ lang })}>
        <Button className="header-btn" shape="circle">
          <GlobalOutlined />
        </Button>
      </Dropdown>
      <Dropdown overlay={menuProfile({ t })}>
        <Button className="header-btn" shape="circle">
          <UserOutlined />
        </Button>
      </Dropdown>
    </div>
  )
}

export default NavHeader
