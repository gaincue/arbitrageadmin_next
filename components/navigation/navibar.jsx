import React, { useState, useEffect, useRef } from "react"
import ReactDOM from "react-dom"
import {
  Layout,
  Button,
  PageHeader,
  Card,
  Menu,
  Affix,
  Dropdown,
  Typography,
  Drawer,
  Row,
  Col
} from "antd"
const { SubMenu } = Menu
const { Header, Content, Sider, Footer } = Layout
import { useRouter } from "next/router"

import { logout, expiredLogout, withAdminSync } from "../../utils/auth"
import { getAuthData } from "../../api/fetch_api"
import NavLinkComponent from "../navigation/nav_link"
import { isEmpty } from "lodash"
import { connect } from "react-redux"
import { setLoginState } from "store"
import styles from "./navibar.less"
import NavHeader from "./navbar_header"
import { openNotification } from "../../utils/notifications"

import Link from "next/link"

import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  HomeOutlined,
  AuditOutlined
} from "@ant-design/icons"
import LoginForm from "../LoginForm"

// import { fetchCompanyName } from "api/company_api";

const { Title } = Typography
let year = new Date().getFullYear()

// special note: As Navibar is used in DefaultLayout, it will receive all the props of the page.
// Available props (user need to set in getInitialProps) are:
// noLoginDrawer: set to true if don't want a Drawer with LoginForm covering the page if not logged in.
// note that loginState prop is get from redux, so don't pass in via getInitialProps.
const Navibar = (props) => {
  const { t, lang } = props
  const router = useRouter()

  const [companyName, setCompanyName] = useState([])
  const [editCompanyVisible, setEditCompanyVisible] = useState(false)

  if (props.loginState) {
    // fetchCompanyDetail();
  }

  const [collapsed, setCollapsed] = useState(false)
  const [selectedKeys, setSelectedKeys] = useState(`1`)

  console.log("reset happen")
  const toggleCollapsed = (e) => {
    setCollapsed(!collapsed)
  }

  useEffect(() => {
    const pathName = router.pathname
    if (pathName == "/trades") {
      setSelectedKeys("2")
    } else if (pathName == "/dashboard") {
      setSelectedKeys("1")
    }
  }, [router])

  return (
    <Layout>
      {!props.noLoginDrawer ? (
        <Layout>
          <Affix>
            <Sider
              breakpoint="lg"
              collapsedWidth="0"
              trigger={null}
              collapsible
              collapsed={collapsed}
              className="menu-style"
            >
              <div className="logo">
                <img src="../../logo.svg" />
              </div>
              <Menu
                mode="inline"
                theme="dark"
                collapsed={collapsed}
                selectedKeys={[selectedKeys]}
                onClick={(value) => {
                  console.log(value)
                  setSelectedKeys(value.key)
                  if (value.key == "1") {
                    router.push("/dashboard")
                  } else if (value.key == "2") {
                    router.push("/trades")
                  }
                }}
              >
                <Menu.Item key="1">
                  <HomeOutlined />
                  <span className="nav-text">{t("common:home")}</span>
                </Menu.Item>
                <Menu.Item key="2">
                  <AuditOutlined />
                  <span className="nav-text">{t("common:dailyProfit")}</span>
                </Menu.Item>
                {/* <Menu.Item
                  key="18"
                  onClick={() => Router.push("/reports")}
                >
                  <LineChartOutlined />
                  <span className="nav-text">Performance Chart</span>
                </Menu.Item> */}
                {/* <Menu.Item
                  key="19"
                  onClick={() => {
                    logout();
                    Router.push("/");
                  }}
                >
                  <LogoutOutlined />
                  <span>Logout</span>
                </Menu.Item> */}
              </Menu>
            </Sider>
          </Affix>
          <Layout className="site-layout">
            <Affix>
              <Header className="site-layout-header">
                <div className="header_nav_button">
                  <Button
                    type="link"
                    size={"large"}
                    onClick={toggleCollapsed}
                    style={{ marginBottom: 16 }}
                  >
                    {React.createElement({ collapsed } ? MenuUnfoldOutlined : MenuFoldOutlined)}
                  </Button>
                  {companyName}
                </div>

                <NavHeader t={t} lang={lang} />
              </Header>
            </Affix>
            <Content className="site-layout-background">{props.children}</Content>
            {/* <Footer className="footer_text">
              Copyright © {year}
            </Footer> */}
          </Layout>
        </Layout>
      ) : (
        props.children
      )}
      <Drawer
        title="Please login to continue"
        visible={!props.loginState && !props.noLoginDrawer}
        placement="top"
        width="100vw"
        height="100vh"
        onClose={() => {
          router.push("/")
        }}
      >
        <Row type="flex" justify="center" align="middle" style={{ minHeight: "80vh" }}>
          <div className="card" style={{ height: "100%", width: "100%" }}>
            <div className="header">
              <LoginForm />
            </div>
          </div>
        </Row>
      </Drawer>
    </Layout>
  )
}

const mapStateToProps = (state) => {
  return { loginState: state.loginState }
}

export default connect(mapStateToProps)(Navibar)
