import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import styled from "styled-components";
import Router from "next/router";
import { Layout, Button, PageHeader, Card, Menu, Affix, Dropdown } from "antd";

// import routes from "../routes"
import { withRouter } from "next/router";

const Paragraph = styled.p.attrs({
  className: "mv-1x nav-menu uppercase"
})`
  letter-spacing: 2.5px;
  text-decoration: none;
  color: #797a7c;
  margin: 0;
`;

const Anchor = styled.div.attrs({
  className: "ph-3x"
})`
  letter-spacing: 1px;
  text-decoration: none;
  color: #797a7c;
  height: 40px;

  &:hover {
    cursor: pointer; 
  }

  &:hover > div > span, &:hover > div > i {
    color: rgb(255, 255, 255) !important;
  }
`;

class NavLinkComponent extends React.Component {
  render() {
    const componentName = "NavLinkComponent";

    const {
      activeClassName,
      className,
      children,
      router,
      ...props
    } = this.props;

    var checkingPath = "/dashboard";

    if (router.asPath != "/") {
      const splitPath = router.asPath.split("/");
      if (splitPath.length >= 3) {
        checkingPath = "/" + splitPath[2];
      }
    }

    const isActiveRoute =
      props.href === undefined ? false : props.href == router.asPath;
    // : router.pathName
    // routes
    //     .findAndGetUrls(props.href, props.params)
    //     .urls.as.replace("/", "")
    //     .startsWith(checkingPath.replace("/", ""));
    // //console.log(props.href)
    // //console.log(checkingPath)

    if (isActiveRoute) {
      //console.log(props.href);
    }

    // return props.href.includes("/") ? (
    return (
      <Anchor
        onClick={() => {
          //console.log("!");
          Router.push(props.href);
        }}
        // {...props}
        className={classNames(componentName, className, {
          [`${activeClassName}`]: isActiveRoute
        })}
      >
        <div
          style={{
            display: "flex",
            // alignContent: "center",
            alignItems: "center",
            height: "100%",
            lineHeight: 1,
            color: "rgba(255, 255, 255, 0.65)"
          }}
        >
          {children}
        </div>
      </Anchor>
    );
    // ) : (
    //   <Paragraph
    //     className={classNames(componentName, className, {
    //       [`${activeClassName}`]: isActiveRoute
    //     })}
    //   >
    //     {children}
    //   </Paragraph>
    // );
  }
}

NavLinkComponent.propTypes = {
  activeClassName: PropTypes.string.isRequired
};

NavLinkComponent.defaultProps = {
  activeClassName: "active"
};

export default withRouter(NavLinkComponent);
