import React, { useEffect, useState } from "react";
import io from "socket.io-client";
import { Line } from "react-chartjs-2";
import useTranslation from "next-translate/useTranslation"

const socketUrl = "https://arbi1.a4de4694.com";

const options = {
  scales: {
    xAxes: [
      {
        display: false
      }
    ],
    yAxes: [
      {
        display: false
      }
    ]
  }
};

const legend = {
  display: false
};

const ProfitCard = ({ duration, color }) => {
  const { t } = useTranslation()
  const [data, setData] = useState({
    datasets: [
      {
        fill: true,
        lineTension: 0,
        backgroundColor: color.replace("b", "ba").replace(")", ", 0.4)"),
        borderColor: color,
        borderCapStyle: "butt",
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        pointRadius: 0,
        pointHitRadius: 0
      }
    ]
  });
  const [profit, setProfit] = useState(0);
  const [profitSet, setProfitSet] = useState([
    0,
    10,
    20,
    30,
    40,
    50,
    40,
    30,
    20,
    10
  ]);

  useEffect(() => {
    const socket = io(socketUrl);
    console.log(duration)
    const profitSelect =
      duration == "1day"
        ? "profit1d"
        : duration == "7days"
        ? "profit7d"
        : "profit";
    socket.on(profitSelect, (data) => {
      // console.log(data);
      setProfit(data);
    });
    socket.on(profitSelect + "List", (data) => {
      // console.log(data);
      setProfitSet(data);
    });
  }, []);

  useEffect(() => {
    data.datasets[0].data = profitSet;
    data.labels = Array.from({ length: profitSet.length }, (_, i) => i);

    setData(data);
  }, [profitSet]);

  return (
    <div className="crypt-dark">
      <div className="crypt-gross-market-cap">
        <div
          style={{
            position: "relative",
            display: "flex",
            justifyContent: "center",
            alignItems: "flex-start",
            flexDirection: "column"
          }}
        >
          <h3 style={{ margin: 0 }}>{t("common:profit")}</h3>
          <div>
            <span style={{ color: color, fontSize: 24 }}>
              {`${new Intl.NumberFormat().format(profit.toFixed(2))}`}
            </span>
            <span
              style={{
                color: color,
                fontSize: 18,
                marginLeft: 4
              }}
            >
              USDT
            </span>
          </div>
          <Line data={data} options={options} legend={legend} height={30} />
          <h3
            style={{
              textAlign: "center",
              margin: 0,
              position: "absolute",
              fontSize: 12,
              top: 0,
              right: -25,
              background: color,
              color: "white",
              padding: "4px 4px 4px 8px",
              borderRadius: "12px 0 0 12px"
            }}
          >
            {t(`common:${duration}`)}
          </h3>
        </div>
      </div>
    </div>
  );
};

export default ProfitCard;
