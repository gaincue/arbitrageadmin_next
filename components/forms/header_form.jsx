import React, { useState } from "react";
import { Form, Input, Row, Col, DatePicker } from "antd";
// import ProTable, { IntlProvider, enUSIntl } from "@ant-design/pro-table"
import styled from "styled-components";

// const HeaderForm = props => {
//   const { columns, handleSave } = props
//   return (
//     <IntlProvider value={enUSIntl}>
//       <ProTable
//         onSubmit={value => {
//           handleSave(value)
//         }}
//         rowKey="id"
//         type="form"
//         columns={columns}
//       />
//     </IntlProvider>
//   )
// }

const Title = styled.p`
  font-size: 30px;
  margin: 0;
  font-weight: bold;
  text-align: right;
`;

const Paragraph = styled(Title)`
  font-size: 16px;
`;

const HeaderForm = (props) => {
  const { form } = props;
  const [balance, setBalance] = useState(0);

  return (
    <Form form={form} layout="vertical">
      <Row align="space-between">
        <Col span={16}>
          <Row gutter={24} justify="end">
            <Col span={12}>
              <Form.Item name="customer" label="Customer">
                <Input />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item name="customer_email" label="Customer Email">
                <Input />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24} justify="end">
            <Col span={6}>
              <Form.Item name="billing_address" label="Billing Address">
                <Input />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item name="terms" label="Terms">
                <Input />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item name="invoice_date" label="Invoice Date">
                {/* <Input /> */}
                <DatePicker />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item name="due_date" label="Due Date">
                <Input />
              </Form.Item>
            </Col>
          </Row>
        </Col>
        <Col span={8}>
          <Row justify="end">
            <Col>
              <Paragraph>Balance</Paragraph>
              <Title>{balance}</Title>
            </Col>
          </Row>
          <Row justify="end" align="bottom">
            <Col>
              &nbsp;
              <Form.Item name="invoice_no" label="Invoice No">
                <Input />
              </Form.Item>
            </Col>
          </Row>
        </Col>
      </Row>
    </Form>
  );
};

export default HeaderForm;
