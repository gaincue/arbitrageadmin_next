import React, { useState } from "react";
import { Form, Input, Row, Col, Select } from "antd";
const { Option } = Select;
// import ProTable, { IntlProvider, enUSIntl } from "@ant-design/pro-table"
import styled from "styled-components";
import Gaps from "../../components/misc/gaps";

// const HeaderForm = props => {
//   const { columns, handleSave } = props
//   return (
//     <IntlProvider value={enUSIntl}>
//       <ProTable
//         onSubmit={value => {
//           handleSave(value)
//         }}
//         rowKey="id"
//         type="form"
//         columns={columns}
//       />
//     </IntlProvider>
//   )
// }

const Title = styled.p`
  font-size: 30px;
  margin: 0;
  font-weight: bold;
  text-align: right;
`;

const Paragraph = styled(Title)`
  font-size: 16px;
`;

const HeaderForm = props => {
  const { form } = props;
  const [balance, setBalance] = useState(0);
  const handleChange = value => {
    console.log(`selected ${value}`);
  };
  return (
    <Form form={form} layout="vertical">
      <Row align="space-between">
        <Col span={16}>
          <Row gutter={24} justify="end">
            <Col span={12}>
              <Form.Item name="message_invoices" label="Message On Invoice">
                <Input />
              </Form.Item>
            </Col>
            <Col span={12}></Col>
          </Row>
          <Row gutter={24} justify="end">
            <Col span={12}>
              <Form.Item name="message_statement" label="Message On Statement">
                <Input />
              </Form.Item>
            </Col>
            <Col span={12}></Col>
          </Row>
          <Row gutter={24} justify="end">
            <Col span={12}>
              <Form.Item name="attachment" label="Attachment">
                <Input />
              </Form.Item>
            </Col>
            <Col span={12}></Col>
          </Row>
        </Col>
        <Col span={8}>
          <Row justify="end">
            <Col>
              <Title> Subtotal </Title>
            </Col>
            <Col>&nbsp;</Col>
            <Col>
              <Title> {balance}</Title>
            </Col>
          </Row>
          <Row justify="end" align="bottom">
            <Col>
              &nbsp;
              <div class="name">
                <Form.Item name="discount_type" label="">
                  <Select
                    defaultValue="0"
                    style={{ width: 180 }}
                    onChange={handleChange}
                  >
                    <Option value="0">Discount Percent</Option>
                    <Option value="1">Discount Value</Option>
                  </Select>
                </Form.Item>
              </div>
            </Col>
            <Col>
              <div class="name">
                <Form.Item name="discount_value" style={{ width: 60 }} label="">
                  <Input />
                </Form.Item>
              </div>
            </Col>
            <Col>&nbsp;</Col>
            <Col>
              <Title>{balance}</Title>
            </Col>
          </Row>
          <Row justify="end">
            <Col>
              <Title> Total </Title>
            </Col>
            <Col>&nbsp;</Col>
            <Col>
              <Title>{balance}</Title>
            </Col>
          </Row>
          <Row justify="end">
            <Col>
              <Title> Balance Due </Title>
            </Col>
            <Col>&nbsp;</Col>
            <Col>
              <Title>{balance}</Title>
            </Col>
          </Row>
        </Col>
      </Row>
    </Form>
  );
};

export default HeaderForm;
