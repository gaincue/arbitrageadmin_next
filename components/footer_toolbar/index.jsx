import React, { Component } from "react";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { handleModalVisible, setSelectedRow } from "store";

import "./index.less";

class FooterToolbar extends Component {
  getWidth = () => {
    // const isMobile = false
    // const siderWidth = 250
    // const sider = document.querySelector(".ant-layout-sider")

    // if (!sider) {
    //   return undefined
    // }

    // return isMobile
    //   ? undefined
    //   : `calc(100% - ${this.props.collapsed ? 80 : siderWidth || 256}px)`

    const { footerWidth } = this.props;

    // return "100vw";
    return footerWidth;
  };

  render() {
    const { children, className, extra, ...restProps } = this.props;

    return (
      <>
        <div
          className="toolbar"
          style={{
            width: this.getWidth(),
            transition: "0.3s all",
          }}
          // {...restProps}
        >
          <div className="left">{extra}</div>
          <div className="right">{children}</div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => state;

export default connect(mapStateToProps, null)(FooterToolbar);
