import useTranslation from "next-translate/useTranslation"
/**
 * A function that returns a handler function, to auto-populate another string
 * form field by copying the current form field value over as it is typed.
 * Use smart algorithm to determine whether to update or not.
 *
 */
const Greet = () => {
  const { t } = useTranslation()
  const hours = new Date().getHours();
  let timeOfDay;

  if (hours < 12) {
    timeOfDay = t("common:goodMorning");
  } else if (hours >= 12 && hours < 17) {
    timeOfDay = t("common:goodAfternoon");
  } else {
    timeOfDay = t("common:goodEvening");
  }

  return timeOfDay;
};

export default Greet;
