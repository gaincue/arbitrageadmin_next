import { Col } from "antd"

export default function WrapperCol(props) {
  return (
    <>
      <Col
        xs={props.span.mobile}
        md={props.span.tablet}
        lg={props.span.desktop}
        style={props.style}
      >
        {props.children}
      </Col>
    </>
  )
}
