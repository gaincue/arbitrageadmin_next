import styled from "styled-components"

const BgDiv = styled.div`
  background: ${props => (props.background ? props.background : "")};

  padding-left: 12px;
  padding-right: 12px;

  @media (min-width: 768px) and (max-width: 1279px) {
    padding-left: 32px;
    padding-right: 32px;
  }
`

export default function WrapperBackground(props) {
  return !props.fullWidth ? (
    <BgDiv background={props.background} {...props}>
      <div style={{ margin: "auto", maxWidth: 1440 }}>
        {props.children}
      </div>
    </BgDiv>
  ) : (
      <BgDiv background={props.background} {...props}>
        {props.children}
      </BgDiv>
    )
}
