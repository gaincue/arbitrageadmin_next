import useTranslation from "next-translate/useTranslation"
/**
 * A function that returns a handler function, to auto-populate another string
 * form field by copying the current form field value over as it is typed.
 * Use smart algorithm to determine whether to update or not.
 *
 */
const GetDate = () => {
  const { t, lang  } = useTranslation()
  const today = new Date();
  const options = { weekday: "long", day: "numeric", month: "long" };

  return today.toLocaleDateString(lang == "en"?"en-US": "zh-CN", options);
};
export default GetDate;
