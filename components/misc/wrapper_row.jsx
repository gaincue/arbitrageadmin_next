import { Row } from "antd"

export default function WrapperRow(props) {
  return (
    <>
      <Row gutter={props.offset || 8} {...props}>
        {props.children}
      </Row>
    </>
  )
}
