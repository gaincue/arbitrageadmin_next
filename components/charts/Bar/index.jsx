import { Axis, Chart, Geom, Tooltip } from "bizcharts"
import React, { Component } from "react"
import Debounce from "lodash.debounce"

import WrapperRow from "../../../components/misc/wrapper_row"
import autoHeight from "../autoHeight"
import "../index.less"

class Bar extends Component {
  state = {
    autoHideXLabels: true
  }

  root = undefined

  node = undefined

  resize = Debounce(() => {
    if (!this.node || !this.node.parentNode) {
      return
    }

    const canvasWidth = this.node.parentNode.clientWidth
    const { data = [], autoLabel = true } = this.props

    if (!autoLabel) {
      return
    }

    const minWidth = data.length * 30
    const { autoHideXLabels } = this.state

    if (canvasWidth <= minWidth) {
      if (!autoHideXLabels) {
        this.setState({
          autoHideXLabels: true
        })
      }
    } else if (autoHideXLabels) {
      this.setState({
        autoHideXLabels: false
      })
    }
  }, 500)

  componentDidMount() {
    window.addEventListener("resize", this.resize, {
      passive: true
    })
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.resize)
  }

  handleRoot = n => {
    this.root = n
  }

  handleRef = n => {
    this.node = n
  }

  render() {
    const {
      height = 1,
      title,
      subtitle,
      yTitle,
      forceFit = true,
      data,
      color = "rgba(24, 144, 255, 0.85)",
      padding
    } = this.props
    const { autoHideXLabels } = this.state
    const scale = {
      x: {
        type: "cat"
      },
      y: {
        alias: yTitle ? yTitle : "USDT",
        min: 0
      }
    }
    const tooltip = [
      "x*y",
      (x, y) => ({
        name: x,
        value: y
      })
    ]
    return (
      <div
        className="chart"
        style={{
          height
        }}
        ref={this.handleRoot}
      >
        <div ref={this.handleRef}>
          {title && (
            <WrapperRow
              type="flex"
              justify="space-between"
              style={{ padding: "0 8px" }}
            >
              {title}
              {subtitle && subtitle}
            </WrapperRow>
          )}
          <Chart
            scale={scale}
            height={title ? height - 41 : height}
            forceFit={forceFit}
            data={data}
            padding={padding || "auto"}
          >
            <Axis
              name="x"
              title={false}
              visible={false}
            />
            <Axis
              name="y"
              title={{
                autoRotate: true,
                offset: 50,
                textStyle: {
                  fontSize: "12",
                  textAlign: "center",
                  fill: "#999",
                  fontWeight: "bold"
                },
                position: "center"
              }}
              min={0}
            />
            <Tooltip showTitle={false} crosshairs={false} />
            <Geom
              type="interval"
              position="x*y"
              color={color}
              tooltip={tooltip}
            />
          </Chart>
        </div>
      </div>
    )
  }
}

export default autoHeight()(Bar)
