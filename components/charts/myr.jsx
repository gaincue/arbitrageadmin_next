import React from "react"
import numeral from "numeral"

export default function MYR({ children }) {
  return <span>RM {numeral(children).format("0,0")}</span>
}
