import { notification, Button } from "antd"
import styled from "styled-components"

const Paragraph = styled.p`
  margin: 0;
  white-space: pre-line;

  .ant-select-selection-selected-value & {
    position: absolute;
    top: 50%;
    left: 16px;
    max-width: 100%;
    height: 20px;
    margin-top: -10px;
    overflow: hidden;
    line-height: 20px;
    white-space: nowrap;
    text-align: left;
    text-overflow: ellipsis;
  }
`

/**
 * Open a slide-in notification panel.
 * @param {*} message 
 * @param {*} description 
 * @param {*} button 
 */
const openNotification = (message, description, button) => {
  const key = `open${Date.now()}`
  const btn = (
    <Button
      type="primary"
      className="label-01 uppercase"
      onClick={() => notification.close(key)}
      style={{ fontSize: "11px" }}
    >
      {button || "OK"}
    </Button>
  )
  notification.open({
    message: <Paragraph className="body-short-02">{message}</Paragraph>,
    description: (
      <Paragraph className="body-short-01">{description}</Paragraph>
    ),
    btn,
    key,
    duration: 15
  })
}

export {openNotification};
