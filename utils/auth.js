import { Component } from "react"
import { parseCookies, setCookie, destroyCookie } from "nookies"

export const login = async (token, user) => {
  setCookie(null, "token", token, {
    maxAge: 24 * 60 * 60, // 2 hours
    path: "/"
  })
  //console.log(token)
  //console.log(user)
  if (user) {
    setCookie(null, "user", JSON.stringify(user), {
      maxAge: 24 * 60 * 60, // 2 hours
      path: "/"
    })
  }

  // Router.push("/dashboard")
}

export const removeCookie = (ctx = null) => {
  if (ctx) {
    destroyCookie(ctx, "token")
    destroyCookie(ctx, "user")    
  } else {
    destroyCookie(null, "token")
    destroyCookie(null, "user")
  }
}

export const logout = () => {
  removeCookie()

  // to support logging out from all windows
  window.localStorage.setItem("logout", Date.now())
  // Router.push("/")
}

export const expiredLogout = ctx => {
  if (ctx.req) {
    removeCookie(ctx)

    ctx.res.writeHead(302, {
      Location: "/?expired=true"
    })
    ctx.res.end()

    return
  } else {
    removeCookie()

    // to support logging out from all windows
    if (window) {
      window.localStorage.setItem("logout", Date.now())
    }

    // Router.push({
    //   pathname: "/?expired=true"
    // })
  }
}

// Gets the display name of a JSX component for dev tools
const getDisplayName = Component =>
  Component.displayName || Component.name || "Component"

export const withAuthSync = WrappedComponent =>
  class extends Component {
    static displayName = `withAuthSync(${getDisplayName(WrappedComponent)})`

    static async getInitialProps(ctx) {
      const token = auth(ctx)

      const componentProps =
        WrappedComponent.getInitialProps &&
        (await WrappedComponent.getInitialProps(ctx))

      return {
        ...componentProps,
        token
      }
    }

    constructor(props) {
      super(props)

      this.syncLogout = this.syncLogout.bind(this)
    }

    componentDidMount() {
      window.addEventListener("storage", this.syncLogout)
    }

    componentWillUnmount() {
      window.removeEventListener("storage", this.syncLogout)
      window.localStorage.removeItem("logout")
    }

    syncLogout(event) {
      if (event.key === "logout") {
        //console.log("logged out from storage!")
        //Router.push("/")
      }
    }

    render() {
      return <WrappedComponent {...this.props} />
    }
  }

export const withAdminSync = WrappedComponent =>
  class extends Component {
    static displayName = `withAdminSync(${getDisplayName(WrappedComponent)})`

    static async getInitialProps(ctx) {
      const token = adminAuth(ctx)

      const componentProps =
        WrappedComponent.getInitialProps &&
        (await WrappedComponent.getInitialProps(ctx))

      return {
        ...componentProps,
        token
      }
    }

    constructor(props) {
      super(props)

      this.syncLogout = this.syncLogout.bind(this)
    }

    componentDidMount() {
      window.addEventListener("storage", this.syncLogout)
    }

    componentWillUnmount() {
      window.removeEventListener("storage", this.syncLogout)
      window.localStorage.removeItem("logout")
    }

    syncLogout(event) {
      if (event.key === "logout") {
        //console.log("logged out from storage!")
        //Router.push("/")
      }
    }

    render() {
      return <WrappedComponent {...this.props} />
    }
  }

export const withAgentSync = WrappedComponent =>
  class extends Component {
    static displayName = `withPRSync(${getDisplayName(WrappedComponent)})`

    static async getInitialProps(ctx) {
      const token = agentAuth(ctx)

      const componentProps =
        WrappedComponent.getInitialProps &&
        (await WrappedComponent.getInitialProps(ctx))

      return {
        ...componentProps,
        token
      }
    }

    constructor(props) {
      super(props)

      this.syncLogout = this.syncLogout.bind(this)
    }

    componentDidMount() {
      window.addEventListener("storage", this.syncLogout)
    }

    componentWillUnmount() {
      window.removeEventListener("storage", this.syncLogout)
      window.localStorage.removeItem("logout")
    }

    syncLogout(event) {
      if (event.key === "logout") {
        //console.log("logged out from storage!")
        //Router.push("/")
      }
    }

    render() {
      return <WrappedComponent {...this.props} />
    }
  }

export const auth = ctx => {
  const { token } = parseCookies(ctx)

  /*
   * This happens on server only, ctx.req is available means it's being
   * rendered on server. If we are on server and token is not available,
   * means user is not logged in.
   */
  if (ctx.req && !token) {
    ctx.res.writeHead(302, {
      Location: "/"
    })
    ctx.res.end()
    return
  }

  // We already checked for server. This should only happen on client.
  if (!token) {
    //Router.push("/")
  }

  return token
}

export const adminAuth = ctx => {
  const { token, user } = parseCookies(ctx)

  if (!user) {
    ctx.res.writeHead(302, {
      Location: "/"
    })
    ctx.res.end()
    return
  }

  const userObj = JSON.parse(user)

  /*
   * This happens on server only, ctx.req is available means it's being
   * rendered on server. If we are on server and token is not available,
   * means user is not logged in.
   */
  if (
    (ctx.req && !token) ||
    userObj.role.name.toLowerCase() != "administrator"
  ) {
    ctx.res.writeHead(302, {
      Location: "/"
    })
    ctx.res.end()
    return
  }

  // We already checked for server. This should only happen on client.
  if (!token || userObj.role.name.toLowerCase() != "administrator") {
    //Router.push("/")
  }

  return token
}

export const agentAuth = ctx => {
  const { token, user } = parseCookies(ctx)

  if (!user) {
    ctx.res.writeHead(302, {
      Location: "/"
    })
    ctx.res.end()
    return
  }

  const userObj = JSON.parse(user)

  /*
   * This happens on server only, ctx.req is available means it's being
   * rendered on server. If we are on server and token is not available,
   * means user is not logged in.
   */
  if (
    (ctx.req && !token) ||
    (userObj.role.name.toLowerCase() != "agent" &&
      userObj.role.name.toLowerCase() != "administrator")
  ) {
    ctx.res.writeHead(302, {
      Location: "/"
    })
    ctx.res.end()
    return
  }

  // We already checked for server. This should only happen on client.
  if (
    !token ||
    (userObj.role.name.toLowerCase() != "agent" &&
      userObj.role.name.toLowerCase() != "administrator")
  ) {
    //Router.push("/")
  }

  return token
}
