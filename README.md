# GIB app frontend with NextJs/ReactJs/AntDesign

## Install it and run:

```bash
npm install
npm run dev
# or
yarn
yarn dev
```

## Recaptcha keys 
Remember to use matching sitekeys (in api/fetch_api.js) when deploying.
For development, can use the special site key that will always succeed.

## Fetching data into view
When fetching data into view via AJAX API, make sure the request is successful and contain data, else it causes undefined/null error in the view and React will not able to render.


# Previous docs (for reference only) :
# Ant Design Example
## Deploy your own

Deploy the example using [ZEIT Now](https://zeit.co/now):

[![Deploy with ZEIT Now](https://zeit.co/button)](https://zeit.co/new/project?template=https://github.com/zeit/next.js/tree/canary/examples/with-ant-design)

## How to use

### Using `create-next-app`

Execute [`create-next-app`](https://github.com/zeit/next.js/tree/canary/packages/create-next-app) with [Yarn](https://yarnpkg.com/lang/en/docs/cli/create/) or [npx](https://github.com/zkat/npx#readme) to bootstrap the example:

```bash
npx create-next-app --example with-ant-design with-ant-design-app
# or
yarn create next-app --example with-ant-design with-ant-design-app
```

### Download manually

Download the example:

```bash
curl https://codeload.github.com/zeit/next.js/tar.gz/canary | tar -xz --strip=2 next.js-canary/examples/with-ant-design
cd with-ant-design
```

Install it and run:

```bash
npm install
npm run dev
# or
yarn
yarn dev
```

Deploy it to the cloud with [now](https://zeit.co/now) ([download](https://zeit.co/download)):

```bash
now
```

## The idea behind the example

This example shows how to use Next.js along with [Ant Design of React](http://ant.design). This is intended to show the integration of this UI toolkit with the Framework.
