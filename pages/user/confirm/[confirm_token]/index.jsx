import { Form } from '@ant-design/compatible';
import '@ant-design/compatible/assets/index.css';
import { Layout, Button, Input } from "antd";
import React, { useState, useEffect } from "react";
import Link from "next/link";
import Router from "next/router";

import Head from "../../../../components/head";
import { loginUser,confirmUser } from "../../../../api/user_api";
import { openNotification } from "../../../../utils/notifications";

const { Content } = Layout;
import { parseCookies } from "nookies";


const head = {
  title: "GIB Admin",
  description: "GIB Software"
};

function confirmUserPage(props) {
  useEffect(() => {
    // console.log(props);
    if (props.success) {
      openNotification(
        "Success",
        "Activate user successfully, please login",
        "OK"
      );
    } else {
      openNotification(
        "Failed",
        "Activate user failed, please try again:" + props.error,
        "OK"
      );
    }
    Router.push("/");
  }, []);

  return (
    <Content>
      <div>
        <img
          src="../../../logo.svg"
          height="250"
          style={{
            position: "absolute",
            left: "40%"
            // top: "50px",
            // marginTop: "-50px"
            // zIndex: 0
          }}
        />
      </div>
    </Content>
  );
}

confirmUserPage.getInitialProps = async ctx => {
  const confirmToken = ctx.query.confirm_token;

  try {
    const resData = await confirmUser(confirmToken);
    
    return resData ;
  } catch (error) {
    if (error.status == 401) {
      expiredLogout(ctx); // TODO what is this?
    }
    //console.log(error);
  }
};

export default confirmUserPage;
