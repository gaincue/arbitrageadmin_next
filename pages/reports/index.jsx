import React, { useEffect, useState } from "react"
import dynamic from "next/dynamic"
import { Layout, Select, Row, Col, DatePicker, Tabs } from "antd"
import { isEmpty } from "lodash"
import moment from "moment"
import DefaultLayout from "../../layout/default_layout"
import {
  getSales
  // getSalesByProduct,
  // getSalesByUnit,
  // getSalesByLocation,
  // getSalesByVending,
  // getSalesByPaymentMethod,
} from "../../api/sales_api"
// import { getProducts } from "api/product_api"
// import { getMachines } from "api/machine_api"
import Head from "../../components/head"
import Gaps from "../../components/misc/gaps"
import WrapperBackground from "../../components/misc/wrapper_background"
import "./style.less"

const { Content } = Layout

const head = {
  title: "GIB Admin",
  description: "GIB Admin Panel"
}

const Bar = dynamic(() => import("../../components/charts/Bar"), {
  ssr: false
})
function Sales(props) {
  const [performance, setPerformance] = useState([])
  const [durationRange, setDurationRange] = useState([moment().add(-1, "days"), moment()])
  // const [productRefreshValue, setProductRefreshValue] = useState(0)
  // const [machineRefreshValue, setMachineRefreshValue] = useState(0)

  const getSalesPerformance = async (range, params) => {
    const sales = await getSales({
      range,
      queryParams: {
        ignorePagination: true
      },
      ...params
    })
    !isEmpty(sales) && setPerformance(sales.data)
  }

  const onChange = (dates, dateStrings) => {
    if (dates == null) {
      dates = []
      dates.push(moment(new Date()))
      dates.push(moment(new Date()))
    }
    setDurationRange([dates[0], dates[1]])
    console.log("From: ", dates[0], ", to: ", dates[1])
    console.log("From: ", dateStrings[0], ", to: ", dateStrings[1])
  }

  useEffect(() => {
    // generateFilters();
  }, [])

  useEffect(() => {
    let params = {}
    getSalesPerformance(durationRange, params)
  }, [durationRange])

  return (
    <>
      <Head title={head.title} description={head.description} />
      <Content style={{ padding: 16 }}>
        <WrapperBackground background="#ffffff" fullWidth>
          <Gaps gaps={{ mobile: "2x", tablet: "", desktop: "" }} />

          <div className="">
            Date Selection{" "}
            <DatePicker.RangePicker
              defaultValue={durationRange}
              ranges={{
                "Last 24 hours": [moment().subtract(1, "days"), moment()],
                "Last 7 days": [moment().subtract(7, "days"), moment()],
                "This Month": [moment().startOf("month"), moment()],
                "Last 30 days": [moment().subtract(30, "days"), moment()],
                "Last Month": [
                  moment().subtract(1, "months").startOf("month"),
                  moment().subtract(1, "months").endOf("month")
                ],
                "Last 6 Months": [moment().subtract(6, "months").startOf("month"), moment()],
                "Last 365 days": [moment().subtract(365, "days"), moment()]
              }}
              onChange={onChange}
            />
          </div>
          <Gaps gaps={{ mobile: "4x", tablet: "", desktop: "" }} />
          <Bar
            height={450}
            title={<h4 style={{ marginBottom: 32 }}>Sales</h4>}
            subtitle={<h4>{performance.reduce((pre, now) => now.y + pre, 0).toFixed(3)} USDT</h4>}
            data={performance}
          />
          <Gaps gaps={{ mobile: "4x", tablet: "", desktop: "" }} />
          {/* </Tabs.TabPane> */}
          {/* </Tabs> */}
        </WrapperBackground>
      </Content>
    </>
  )
}

export default Sales
