import "@ant-design/compatible/assets/index.css";
import {
  Layout,
  Button,
  Input,
  PageHeader,
  Form,
  Row,
  Col,
  message
} from "antd";
import {
  MailOutlined,
  UserOutlined,
  LockOutlined,
  PhoneOutlined
} from "@ant-design/icons";
import React, { useState, useEffect, useRef } from "react";
import Link from "next/link";
import Router from "next/router";

import Head from "../components/head";
import { loginUser, registerUser } from "../api/user_api";
// import { recaptchaSiteKey } from "api/fetch_api";
import { removeCookie } from "../utils/auth";
import { openNotification } from "../utils/notifications";
import Recaptcha from "reaptcha";

const { Header, Content } = Layout;

const head = {
  title: "GIB Admin",
  description: "GIB software"
};

function RegisterForm(props) {
  //const formRef = useRef(null);
  const [loading, setLoading] = useState(false);
  const [confirmDirty, setConfirmDirty] = useState(false);
  const [rcptResult, setRcptResult] = useState(""); //recaptcha validation results

  const recaptchaVerifyCb = async token => {
    //openNotification("Recaptcha","Token is: "+JSON.stringify(token));
    setRcptResult(token); //TODO: why sometimes token is not set???
  };

  async function handleSubmit(values) {
    setLoading(true);

    const firstName = values.firstName;
    const lastName = values.lastName;
    const email = values.email;
    const password = values.password;
    const recaptchaToken = rcptResult;

    const result = await registerUser(
      firstName,
      lastName,
      password,
      email,
      recaptchaToken
    );

    if (result.success) {
      message.success("Registered Successfully");
      Router.push("/");
      //formRef.current.resetFields();
    } else {
      openNotification("Error", result.error, "OK");
    }

    setLoading(false);
  }

  return (
    <>
      <Layout>
        <Head title={head.title} description={head.description} />
        {/* <Header>
          <PageHeader onBack={() => Router.back()} title="Register new user" />
        </Header> */}
        <Content>
          <Row
            style={{
              maxWidth: "85vw",
              marginTop: "50px",
              margin: "auto",
              height: "100vh"
              //display: "flex",
              //flexDirection: "column",
              //justifyContent: "center"
            }}
            type="flex"
            align="middle"
          >
            <Col offset={2} span={10}>
              <div class="card" style={{ height: "90%" }}>
                <div class="header">
                  <div style={{ textAlign: "center" }}>
                    <h1> Sign up for a 30 day trial! </h1>
                  </div>
                  <Form
                    onFinish={handleSubmit}
                    labelCol={{ span: 24 }}
                    wrapperCol={{ span: 24 }}
                  >
                    <Form.Item
                      label="Email"
                      style={{ marginTop: 5, marginBottom: 5 }}
                      name="email"
                      rules={[
                        {
                          type: "email",
                          message: "The email address you entered is not valid"
                        },
                        {
                          required: true,
                          message: "Please enter your email address"
                        }
                      ]}
                    >
                      <Input
                        prefix={
                          <MailOutlined style={{ color: "rgba(0,0,0,.25)" }} />
                        }
                        placeholder="Email address"
                      />
                    </Form.Item>
                    <Form.Item
                      label="First Name"
                      style={{ marginTop: 5, marginBottom: 5 }}
                      name="firstName"
                      rules={[
                        {
                          required: true,
                          message: "Please enter your first name"
                        }
                      ]}
                    >
                      <Input
                        prefix={
                          <UserOutlined style={{ color: "rgba(0,0,0,.25)" }} />
                        }
                        placeholder="First name"
                      />
                    </Form.Item>
                    <Form.Item
                      label="Last Name"
                      style={{ marginTop: 5, marginBottom: 5 }}
                      name="lastName"
                      rules={[
                        {
                          required: true,
                          message: "Please enter your last name"
                        }
                      ]}
                    >
                      <Input
                        prefix={
                          <UserOutlined style={{ color: "rgba(0,0,0,.25)" }} />
                        }
                        placeholder="Last name"
                      />
                    </Form.Item>
                    <Form.Item
                      label="Phone no"
                      style={{ marginTop: 5, marginBottom: 5 }}
                      name="phone"
                      rules={[
                        {
                          required: true,
                          message: "Please enter your phone number"
                        }
                      ]}
                    >
                      <Input
                        prefix={
                          <PhoneOutlined style={{ color: "rgba(0,0,0,.25)" }} />
                        }
                        placeholder="Phone no"
                      />
                    </Form.Item>
                    <Form.Item
                      label="Password"
                      hasFeedback
                      name="password"
                      rules={[
                        {
                          required: true,
                          message: "Please enter your password"
                        },
                        {
                          min: 10,
                          message:
                            "Your password is too short. Minimum length is 10."
                        },
                        {
                          validator: (rule, value, callback) => {
                            {
                              /* debugger; */
                            }
                            const form = props.form;
                            if (value && confirmDirty) {
                              form.validateFields(["confirm"], { force: true });
                            }
                            callback();
                          }
                        }
                      ]}
                    >
                      <Input.Password
                        prefix={
                          <LockOutlined style={{ color: "rgba(0,0,0,.25)" }} />
                        }
                        placeholder="Password"
                      />
                    </Form.Item>
                    <Form.Item
                      label="Confirm Password"
                      hasFeedback
                      name="confirm"
                      dependencies={["password"]}
                      rules={[
                        {
                          required: true,
                          message: "Please confirm your password"
                        },
                        ({ getFieldValue }) => ({
                          validator(rule, value) {
                            if (!value || getFieldValue("password") === value) {
                              return Promise.resolve();
                            }
                            return Promise.reject("The passwords do not match");
                          }
                        })
                      ]}
                    >
                      <Input.Password
                        prefix={
                          <LockOutlined style={{ color: "rgba(0,0,0,.25)" }} />
                        }
                        placeholder="Re-enter password"
                      />
                    </Form.Item>
{/* 
                    <Form.Item>
                      <div align="center">
                        <Recaptcha
                          sitekey={recaptchaSiteKey}
                          onVerify={recaptchaVerifyCb}
                        />
                      </div>
                    </Form.Item> */}

                    <Form.Item style={{ marginTop: 4, marginBottom: 4 }}>
                      {/*Submit button. Centered with col ratio of 5-14-5*/}
                      <Row style={{ textAlign: "center" }}>
                        <Col offset={5} span={14}>
                          <Button
                            block
                            type="primary"
                            htmlType="submit"
                            className="register-form-button"
                            block
                            loading={loading}
                          >
                            {/*TODO: gray out button when recaptcha is not done yet!*/}
                            Sign up{" "}
                          </Button>
                        </Col>
                      </Row>
                    </Form.Item>
                  </Form>
                </div>
              </div>
            </Col>

            <Col offset={1} span={10}>
              <div>
                <div class="header" style={{ background: "#f0f0f0" }}>
                  <div style={{ textAlign: "center" }}>
                    <img
                      src="logo.svg"
                      height="80"
                      // width="200"
                      style={{
                        left: "10%",
                        marginTop: "10px"
                        // zIndex: 0
                      }}
                    />
                  </div>
                  <div>
                    <p>
                      {" "}
                      <h3 style={{ textAlign: "center", marginTop: "30px" }}>
                        Your Online Plan
                      </h3>
                      What is Lorem Ipsum? Lorem Ipsum is simply dummy text of
                      the printing and typesetting industry. Lorem Ipsum has
                      been the industry's standard dummy text ever since the
                      1500s, when an unknown printer took a galley of type and
                      scrambled it to make a type specimen book. It has survived
                      not only five centuries, but also the leap into electronic
                      typesetting, remaining essentially unchanged.{" "}
                    </p>
                    <ul>
                      <li>Automatic data back-ups</li>
                      <li>Bank-level security and encryption</li>
                      <li>Access data from all your devices</li>
                    </ul>
                    <p>
                      It was popularised in the 1960s with the release of
                      Letraset sheets containing Lorem Ipsum passages, and more
                      recently with desktop publishing software like Aldus
                      PageMaker including versions of Lorem Ipsum. Why do we use
                      it? It is a long established fact that a reader will be
                      distracted by the readable content of a page when looking
                      at its layout. The point of using Lorem Ipsum is that it
                      has a more-or-less normal distribution of letters, as
                      opposed to using 'Content here, content here', making it
                      look like readable English. Many desktop publishing
                      packages and web page editors now use Lorem Ipsum as their
                      default model text, and a search for 'lorem ipsum' will
                      uncover many web sites still in their infancy.
                    </p>{" "}
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </Content>
      </Layout>
    </>
  );
}

RegisterForm.getInitialProps = async ctx => {
  if (ctx && ctx.query.expired) {
    removeCookie(ctx);

    return { expired: true, noLoginDrawer: true };
  }

  return {
    expired: false,
    noLoginDrawer: true
  };
};

export default RegisterForm;
