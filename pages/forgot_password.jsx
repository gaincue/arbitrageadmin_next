import { Form, Icon as LegacyIcon } from "@ant-design/compatible"
import "@ant-design/compatible/assets/index.css"
import { Layout, Button, Input, Row, Col, message } from "antd"
import { LockOutlined, UserOutlined } from "@ant-design/icons"
import styled from "styled-components"
import React, { useState, useEffect } from "react"
import Link from "next/link"
import Router from "next/router"

import Head from "../components/head"
import { loginUser } from "../api/user_api"
import { removeCookie } from "../utils/auth"
import { openNotification } from "../utils/notifications"

const { Content } = Layout
import { baseUrl } from "../api/fetch_api"

const head = {
  title: "GIB Admin",
  description: "GIB software",
}

const ResponsiveDiv = styled.div`
  height: 100%;
  width: 80%;

  @media (min-width: 750px) {
    width: 60%;
  }
`

function LoginForm(props) {
  const [loading, setLoading] = useState(false)
  const { getFieldDecorator } = props.form

  useEffect(() => {
    if (props.expired) {
      openNotification("Expired", "Your session has expired", "OK")
    }
  }, [])

  async function handleSubmit(event) {
    event.preventDefault()

    setLoading(true)

    props.form.validateFields(async (err, values) => {
      if (!err) {
        // console.log("Received values of form: ", values);

        const email = values.email
        try {
          const response = await fetch(`${baseUrl}/user/reset-password`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              email: email,
            }),
          })

          if (!response.ok) {
            const res = await response.json()
            // console.log(res);
            // openNotification("Error", res.message, "OK");
            message.error(res.message)
            throw "not ok"
          }

          const res = await response.json()
          // console.log(res);
          if (res.success) {
            openNotification("Success", res.message, "OK")
            Router.push("/")
          } else {
            // openNotification("Failed", res.message, "OK");
            message.error(res.message)
          }
          setLoading(false)
          props.form.resetFields()
          setImageUrl(null)
        } catch (error) {
          // Handle error.
          // console.log("An error occurred:", error);
          setLoading(false)
        }
        setLoading(false)
      } else {
        setLoading(false)
      }
    })
  }

  return (
    <>
      <Layout>
        <Head title={head.title} description={head.description} />
        <Content>
          <Row
            type="flex"
            justify="center"
            align="middle"
            style={{ minHeight: "100vh" }}
          >
            <ResponsiveDiv className="card">
              <div className="header">
                <img
                  src="logo.svg"
                  style={{ height: "100px", margin: "20px" }}
                />
                <Form onSubmit={handleSubmit}>
                  <Form.Item style={{ marginTop: 24, marginBottom: 8 }}>
                    {getFieldDecorator("email", {
                      rules: [
                        {
                          type: "email",
                          message: "Please enter a valid email.",
                        },
                        {
                          required: true,
                          message: "Please enter a valid email.",
                        },
                      ],
                    })(
                      <Col
                        xs={24}
                        lg={24}
                        style={{ textAlign: "center", width: "100vw" }}
                      >
                        <Input
                          prefix={
                            <UserOutlined
                              type="email"
                              style={{ color: "rgba(0,0,0,.25)" }}
                            />
                          }
                          placeholder="Email"
                        />
                      </Col>
                    )}
                  </Form.Item>
                  <div
                    style={{
                      display: "flex",
                      width: "100%",
                      justifyContent: "space-between",
                    }}
                  >
                    <Button
                      type="ghost"
                      href="/"
                      className="login-form-button"
                      loading={loading}
                    >
                      Cancel
                    </Button>
                    <Button
                      type="primary"
                      htmlType="submit"
                      className="login-form-button"
                      loading={loading}
                    >
                      Reset password
                    </Button>
                  </div>
                </Form>
                {/* </div> */}
              </div>
            </ResponsiveDiv>
          </Row>
        </Content>
      </Layout>
    </>
  )
}

LoginForm.getInitialProps = async (ctx) => {
  if (ctx && ctx.query.expired) {
    removeCookie(ctx)

    return { expired: true, noLoginDrawer: true }
  }

  return { expired: false, noLoginDrawer: true }
}

const Admin = Form.create()(LoginForm)
export default Admin
