import React, { useEffect, useState } from "react"
import dynamic from "next/dynamic"
import { Layout, Select, Row, Col, Typography, Tabs, Table } from "antd"
import { isEmpty } from "lodash"
import moment from "moment"
import { compose } from "redux"
import useTranslation from "next-translate/useTranslation"
import DefaultLayout from "../../layout/default_layout"
import { getSales, getTrades } from "../../api/sales_api"
// import { getProducts } from "api/product_api"
// import { getMachines } from "api/machine_api"
import Head from "../../components/head"
import Gaps from "../../components/misc/gaps"
import WrapperBackground from "../../components/misc/wrapper_background"
import MYR from "../../components/charts/myr"
import "./style.less"
const DynamicTable = dynamic(() => import("../../components/table/dynamic_table"), {
  ssr: false
})
const { Content } = Layout

const head = {
  title: "GIB Admin",
  description: "GIB Admin Panel"
}

function Trades(props) {
  const { t } = useTranslation()
  const tradeColumns = [
    {
      title: t("common:date"),
      dataIndex: "time",
      key: "time",
      render: (value) => moment(value).format("YYYY-MM-DD")
    },
    {
      title: t("common:totalProfit(USDT)"),
      dataIndex: "total",
      key: "total"
    }
  ]

  return (
    <>
      <Head title={head.title} description={head.description} />
      <Content style={{ padding: 12 }}>
        <WrapperBackground background="#ffffff" fullWidth>
          <Row gutter={[2, 2]} type="flex">
            <Col span={24}>
              <DynamicTable
                title={t("common:dailyProfit")}
                getData={getTrades}
                columns={tradeColumns}
                params={{
                  _sort: "time:asc"
                }}
                defaultSort="time:asc"
                disablePagination={true}
                scroll={{ x: 300, y: 400 }}
              />
            </Col>
          </Row>

          <Gaps gaps={{ mobile: "4x", tablet: "", desktop: "" }} />
        </WrapperBackground>
      </Content>
    </>
  )
}

export default Trades
