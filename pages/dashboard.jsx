import { Card, Typography, Row, Col, DatePicker } from "antd"

const { Title, Text } = Typography

import { useState, useEffect, useRef } from "react"
import { parseCookies } from "nookies"
import { useRouter } from "next/router"
import styled from "styled-components"
import { isEmpty } from "lodash"
import useTranslation from "next-translate/useTranslation"

import DefaultLayout from "../layout/default_layout"
import Head from "../components/head"

import React from "react"
import dynamic from "next/dynamic"
import WrapperRow from "../components/misc/wrapper_row"
import WrapperCol from "../components/misc/wrapper_col"
import WrapperBackground from "../components/misc/wrapper_background"
import moment from "moment"
import { CaretUpOutlined, CaretDownOutlined } from "@ant-design/icons"
import Gaps from "../components/misc/gaps"

import GetDate from "../components/misc/date"
import Greet from "../components/misc/greet"
import ProfitCard from "../components/elements/profit_card"
const FlexCard = styled(Card)`
  height: 100%;
`

import { getSales, getSalesByExchange } from "../api/sales_api"

// const { Header, Content, Sider, Image } = Layout;
const Bar = dynamic(() => import("../components/charts/Bar"), {
  ssr: false
})
const Pie = dynamic(() => import("../components/charts/Pie"), {
  ssr: false
})
const head = {
  title: "GIB Dashboard",
  description: "GIB Admin panel"
}

const visitData2 = []

const CardItem = styled(Card.Grid)`
  :hover {
    cursor: pointer;
  }
`

const closeEditCompany = (key) => {
  const router = useRouter()
  router.push(`${Router.pathname}`)
}

export default function Dashboard(props) {
  // const [editCompanyVisible, setEditCompanyVisible] = useState(false);
  const { t } = useTranslation()
  const [exchangeFromSales, setExchangeFromSales] = useState([])
  const [exchangeToSales, setExchangeToSales] = useState([])

  const getExchangeSales = async () => {
    const sales = await getSalesByExchange({ exchangeDir: "from_exchange" })
    if (isEmpty(sales)) {
      setExchangeFromSales([])
    } else {
      setExchangeFromSales(sales.processedData)
    }

    const sales2 = await getSalesByExchange({ exchangeDir: "to_exchange" })
    if (isEmpty(sales2)) {
      setExchangeToSales([])
    } else {
      setExchangeToSales(sales2.processedData)
    }
  }
  const [performance, setPerformance] = useState([])
  const [durationRange, setDurationRange] = useState([moment().subtract(1, "days"), moment()])
  // const [productRefreshValue, setProductRefreshValue] = useState(0)
  // const [machineRefreshValue, setMachineRefreshValue] = useState(0)

  const getSalesPerformance = async (range, params) => {
    const sales = await getSales({
      range,
      queryParams: {
        ignorePagination: true
      },
      ...params
    })
    !isEmpty(sales) && setPerformance(sales.data)
  }

  const onChange = (dates, dateStrings) => {
    if (dates == null) {
      dates = []
      dates.push(moment(new Date()))
      dates.push(moment(new Date()))
    }
    setDurationRange([dates[0], dates[1]])
    console.log("From: ", dates[0], ", to: ", dates[1])
    console.log("From: ", dateStrings[0], ", to: ", dateStrings[1])
  }

  useEffect(() => {
    // useEffect cannot accept a function that return a promise (including async function)
    // so have to define an async func and call directly
    // fetchCompanyDetail();
    getExchangeSales()
  }, []) // trigger this effect on first render
  useEffect(() => {
    let params = {}
    getSalesPerformance(durationRange, params)
  }, [durationRange])

  return (
    <>
      <div>
        <Title level={4}>
          <a onClick={() => setEditCompanyVisible(true)}>{/* {props.user.company_name}*/}</a>
        </Title>
        <Text strong>
          {Greet()}, {props.user.username?.toUpperCase() ?? ""} . {t("common:todayIs")} {GetDate()}
        </Text>
      </div>
      <Gaps gaps={{ mobile: "4x", tablet: "", desktop: "" }} />
      <div className="site-card-wrapper ">
        <WrapperRow offset={[16, 16]} type="flex">
          <WrapperCol span={{ mobile: 24, desktop: 8 }}>
            <FlexCard className="card">
              <ProfitCard duration={"1day"} color="rgb(83, 174, 148)" />
            </FlexCard>
          </WrapperCol>
          <Gaps gaps={{ mobile: "4x", tablet: "", desktop: "" }} />
          <WrapperCol span={{ mobile: 24, desktop: 8 }}>
            <FlexCard className="card">
              <ProfitCard duration={"7days"} color="rgb(242, 75, 75)" />
            </FlexCard>
          </WrapperCol>
          <Gaps gaps={{ mobile: "4x", tablet: "", desktop: "" }} />
          <WrapperCol span={{ mobile: 24, desktop: 8 }}>
            <FlexCard className="card">
              <ProfitCard duration={"toDate"} color="rgb(174, 83, 136)" />
            </FlexCard>
          </WrapperCol>
        </WrapperRow>
      </div>
      <Gaps gaps={{ mobile: "4x", tablet: "", desktop: "" }} />

      {!isEmpty(exchangeFromSales) ? (
        <>
          <WrapperBackground background="#ffffff" fullWidth>
            <WrapperRow offset={[16, 16]} type="flex">
              <WrapperCol span={{ mobile: 24, desktop: 12 }}>
                <Pie
                  hasLegend
                  // subTitle={"Profit Rate by Exchange"}
                  // total={exchangeFromSales.reduce((pre, now) => now.y + pre, 0)}
                  title={<h4 style={{ marginTop: "10px" }}>{t("common:exchangeBuyFrequency")}</h4>}
                  data={exchangeFromSales}
                  valueFormat={(value) => {
                    value
                  }}
                  height={400}
                  lineWidth={4}
                />{" "}
              </WrapperCol>
              <WrapperCol span={{ mobile: 24, desktop: 12 }}>
                <Pie
                  hasLegend
                  title={<h4 style={{ marginTop: "10px" }}>{t("common:exchangeSellFrequency")}</h4>}
                  // subTitle={"Profit Rate by Exchange"}
                  // total={exchangeFromSales.reduce((pre, now) => now.y + pre, 0)}
                  data={exchangeToSales}
                  valueFormat={(value) => {
                    value
                  }}
                  height={400}
                  lineWidth={4}
                />{" "}
              </WrapperCol>
            </WrapperRow>
          </WrapperBackground>{" "}
          <Gaps gaps={{ mobile: "4x", tablet: "", desktop: "" }} />
        </>
      ) : (
        <></>
      )}
      <WrapperBackground background="#ffffff" fullWidth>
        <Gaps gaps={{ mobile: "4x", tablet: "", desktop: "" }} />
        <div className="">
          {t("common:dateSelection")}{" "}
          <DatePicker.RangePicker
            defaultValue={durationRange}
            ranges={{
              "Last 24 hours": [moment().subtract(1, "days"), moment()],
              "Last 7 days": [moment().subtract(7, "days"), moment()],
              "This Month": [moment().startOf("month"), moment()],
              "Last 30 days": [moment().subtract(30, "days"), moment()],
              "Last Month": [
                moment().subtract(1, "months").startOf("month"),
                moment().subtract(1, "months").endOf("month")
              ],
              "Last 6 Months": [moment().subtract(6, "months").startOf("month"), moment()],
              "Last 365 days": [moment().subtract(365, "days"), moment()]
            }}
            onChange={onChange}
          />
        </div>
        <Gaps gaps={{ mobile: "4x", tablet: "", desktop: "" }} />
        <Bar
          height={450}
          title={<h4 style={{ marginBottom: 32 }}>{t("common:sales")}</h4>}
          subtitle={<h4>{performance.reduce((pre, now) => now.y + pre, 0).toFixed(3)} USDT</h4>}
          data={performance}
        />
        <Gaps gaps={{ mobile: "4x", tablet: "", desktop: "" }} />
      </WrapperBackground>
    </>
  )
}

Dashboard.getInitialProps = async (ctx) => {
  const { token, user, company } = parseCookies(ctx)
  var userObj = ""

  if (!isEmpty(user) && user != "undefined") {
    userObj = JSON.parse(user)
  }

  return { token, user: userObj }
}
