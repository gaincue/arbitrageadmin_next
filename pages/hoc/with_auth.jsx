import React, { Component } from "react"
import { parseCookies } from "nookies"
import { isEmpty } from "lodash"

export default function withAuth(AuthComponent) {
  return class Authenticated extends Component {
    static async getInitialProps(ctx) {
      const { token, user, companies } = parseCookies(ctx)
      let userObj = {}
      let company = {}

      if (!isEmpty(user) && user != "undefined") {
        userObj = JSON.parse(user)
      }

      if (!isEmpty(companies) && companies != "undefined") {
        company = JSON.parse(companies)[0]
      }

      // Check if Page has a `getInitialProps`; if so, call it.
      const pageProps =
        AuthComponent.getInitialProps &&
        (await AuthComponent.getInitialProps(ctx))
      return { ...pageProps, token, user: userObj, company }
    }

    // componentDidMount() {
    //   if (!Auth.loggedIn()) {
    //     Router.push("/")
    //   }
    //   this.setState({ isLoading: false })
    // }

    render() {
      return <AuthComponent {...this.props} />
    }
  }
}
