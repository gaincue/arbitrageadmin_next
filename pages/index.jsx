import { ForwardOutlined } from "@ant-design/icons"
import "@ant-design/compatible/assets/index.css"
import { Layout, Affix, Row, Col, Button, Input } from "antd"
import React, { useState, useEffect } from "react"
import Link from "next/link"
import { useRouter } from "next/router"
import styled from "styled-components"

import Head from "../components/head"
import LoginForm from "../components/LoginForm"

import { removeCookie } from "../utils/auth"
import { openNotification } from "../utils/notifications"
import { getAuthData } from "../api/fetch_api"

const { Content } = Layout
const head = {
  title: "Account Admin",
  description: "GIB software"
}

const ResponsiveDiv = styled.div`
  height: 100%;
  width: 80%;

  @media (min-width: 750px) {
    width: 60%;
  }
`

function MainPage(props) {
  const router = useRouter()
  // redirect to dashboard if already logged in
  useEffect(() => {
    if (getAuthData().token && getAuthData().token != "") {
      router.push("/dashboard")
    }
  }, [])

  return (
    <>
      {/* Top level cannot have <div> so use React Fragment syntax here */}
      <Layout>
        <Head title={head.title} description={head.description} />
        {/* Note for login page, set height to approx 100vh to fill entire page. 
          Else remove height property for other pages.*/}
        <Content>
          <Row type="flex" justify="center" align="middle" style={{ minHeight: "100vh" }}>
            <ResponsiveDiv className="card">
              <div className="header">
                <LoginForm
                  onLoginSuccess={() => {
                    router.push("/dashboard")
                  }}
                />
              </div>
            </ResponsiveDiv>
          </Row>
        </Content>
      </Layout>
    </>
  )
}

MainPage.getInitialProps = () => {
  // By returning { props: ... }, the  component
  // will receive prop at build time
  return {
    noLoginDrawer: true
  }
}

export default MainPage
