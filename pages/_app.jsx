import React from "react"
import NProgress from "nprogress"
import App from "next/app"
import Router from "next/router"
import withRedux from "next-redux-wrapper"
import { Provider } from "react-redux"
import { compose } from "redux"

import { initStore } from "../store"
import DefaultLayout from "../layout/default_layout"
import axios from "axios"

import "../variables.less"
// import "../styles.css"
import "../nprogress.css"

Router.events.on("routeChangeStart", (url) => {
  NProgress.start()
})
Router.events.on("routeChangeComplete", () => NProgress.done())
Router.events.on("routeChangeError", () => NProgress.done())

class MyApp extends App {
  render() {
    const { Component, pageProps, store } = this.props
    console.log(pageProps)
    // interceptor to handle expired user token / unauthorized
    // yeah, i know, but i need a redux store to dispatch the login state!
    axios.interceptors.response.use(
      function (response) {
        // Any status code that lie within the range of 2xx cause this function to trigger
        // Do something with response data
        return response
      },
      function (error) {
        // Any status codes that falls outside the range of 2xx cause this function to trigger
        // Do something with response error
        if (error.response.status == 401) {
          // set global loginState to false (to trigger the login Drawer)
          store.dispatch({ type: "SET_LOGIN_STATE", loginState: false })
        }
        return Promise.reject(error)
      }
    )
    return (
      <Provider store={store}>
        <DefaultLayout {...pageProps}>
          <Component {...pageProps} />
        </DefaultLayout>
      </Provider>
    )
  }
}

export default withRedux(initStore)(MyApp)
