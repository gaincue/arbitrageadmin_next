import "@ant-design/compatible/assets/index.css"
import { Layout, Button, Input, Form, Row, Col } from "antd"
import React, { useState, useEffect } from "react"
import Link from "next/link"
import Router from "next/router"
import styled from "styled-components"

import Head from "../../../components/head"
import { loginUser, registerUser } from "../../../api/user_api"
import { removeCookie } from "../../../utils/auth"
import { parseCookies } from "nookies"
import { openNotification } from "../../../utils/notifications"

const { Header, Content } = Layout
import { baseUrl } from "../../../api/fetch_api"
import { logout, expiredLogout, withAdminSync } from "../../../utils/auth"

const head = {
  title: "GIB Admin",
  description: "GIB software",
}

const ResponsiveDiv = styled.div`
  height: 100%;
  width: 60%;
  padding: 16px;
  margin: auto;
`

function ChangePasswordForm(props) {
  const [loading, setLoading] = useState(false)
  const [confirmDirty, setConfirmDirty] = useState(false)

  useEffect(() => {
    // if (props.expired) {
    //   openNotification("Expired", "Your session has expired", "OK");
    // }
  }, [])

  async function handleSubmit(event) {
    event.preventDefault()

    setLoading(true)

    props.form.validateFields(async (err, values) => {
      if (!err) {
        // console.log("Received values of form: ", values);
        const currentPassword = values.password
        const newPassword = values.new_password
        const token = props.token
        // console.log(token)
        try {
          const response = await fetch(`${baseUrl}/user/change-password`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${token}`,
            },
            body: JSON.stringify({
              password: currentPassword,
              password_new: newPassword,
            }),
          })

          if (!response.ok) {
            const res = await response.json()
            // console.log(res);
            openNotification("Error", res.message, "OK")
            throw "not ok"
          }

          const res = await response.json()
          // console.log(res);
          if (res.success) {
            openNotification("Success", res.message, "OK")
            logout()
          } else {
            openNotification("Failed", res.message, "OK")
          }
          setLoading(false)
          props.form.resetFields()
          // setImageUrl(null);
        } catch (error) {
          // Handle error.
          // console.log("An error occurred:", error);
          setLoading(false)
        }
        setLoading(false)
      } else {
        setLoading(false)
      }
    })
  }

  return (
    <>
      <Head title={head.title} description={head.description} />
      <Content>
        <ResponsiveDiv className="card">
          <div>
            <img src="../../logo.svg" height="200" width="200" />
          </div>

          <Form
            onSubmit={handleSubmit}
            style={{
              margin: "auto",
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              textAlign: "center",
            }}
          >
            <Form.Item
              hasFeedback
              name="password"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
                {
                  validator: (rule, value, callback) => {
                    {
                      /* debugger; */
                    }
                    const form = props.form
                    if (value && confirmDirty) {
                      form.validateFields(["confirm"], { force: true })
                    }
                    callback()
                  },
                },
              ]}
            >
              <Input.Password placeholder="Existing password" />
            </Form.Item>
            <Form.Item
              hasFeedback
              name="new_password"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
                {
                  validator: (rule, value, callback) => {
                    {
                      /* debugger; */
                    }
                    const form = props.form
                    if (value && confirmDirty) {
                      form.validateFields(["confirm"], { force: true })
                    }
                    callback()
                  },
                },
              ]}
            >
              <Input.Password placeholder="New Password" />
            </Form.Item>
            <Form.Item
              hasFeedback
              name="confirm"
              rules={[
                {
                  required: true,
                  message: "Please confirm your password!",
                },
                {
                  validator: (rule, value, callback) => {
                    const form = props.form
                    if (value && value !== form.getFieldValue("new_password")) {
                      callback("Two passwords that you enter is inconsistent!")
                    } else {
                      callback()
                    }
                  },
                },
              ]}
            >
              <Input.Password
                placeholder="Confirm Password"
                onBlur={(e) => {
                  const value = e.target.value
                  //console.log(value);
                  setConfirmDirty(confirmDirty || !!value)
                }}
              />
            </Form.Item>

            <Form.Item>
              <div style={{ textAlign: "right" }}>
                <Button type="primary" htmlType="submit">
                  Change password
                </Button>
              </div>
            </Form.Item>
          </Form>
        </ResponsiveDiv>
      </Content>
    </>
  )
}

ChangePasswordForm.getInitialProps = async (ctx) => {
  const { token } = parseCookies(ctx)
  if (ctx && ctx.query.expired) {
    removeCookie(ctx)

    return { token: token, expired: true }
  }

  return { token: token, expired: false }
}

export default ChangePasswordForm
