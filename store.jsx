import { createStore, applyMiddleware } from "redux"
import { composeWithDevTools } from "redux-devtools-extension"
import thunkMiddleware from "redux-thunk"

const initialState = {
  collapsed: false,
  modalVisible: false,
  selectedRow: {},
  loginState: false
}

export const actionTypes = {
  TOGGLE_COLLAPSED: "TOGGLE_COLLAPSED",
  HANDLE_MODAL_VISIBLE: "HANDLE_MODAL_VISIBLE",
  SET_SELECTED_ROW: "SET_SELECTED_ROW",
  SET_LOGIN_STATE: "SET_LOGIN_STATE"
}

// REDUCERS
export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.TOGGLE_COLLAPSED:
      return Object.assign({}, state, {
        collapsed: !state.collapsed
      })
    case actionTypes.HANDLE_MODAL_VISIBLE:
      return Object.assign({}, state, {
        modalVisible: action.isVisible
      })
    case actionTypes.SET_SELECTED_ROW:
      return Object.assign({}, state, {
        selectedRow: action.selectedRow
      })
    case actionTypes.SET_LOGIN_STATE:
      return Object.assign({}, state, {
        loginState: action.loginState
      })
    default:
      return state
  }
}

// ACTIONS
export const toggleCollapsed = () => dispatch => {
  return dispatch({ type: actionTypes.TOGGLE_COLLAPSED })
}
export const handleModalVisible = isVisible => dispatch => {
  return dispatch({ type: actionTypes.HANDLE_MODAL_VISIBLE, isVisible })
}
export const setSelectedRow = selectedRow => dispatch => {
  return dispatch({ type: actionTypes.SET_SELECTED_ROW, selectedRow })
}

export const setLoginState = loginState => dispatch => {
  return dispatch({ type: actionTypes.SET_LOGIN_STATE, loginState })
}

export const initStore = initialState => {
  return createStore(
    reducer,
    initialState,
    composeWithDevTools(applyMiddleware(thunkMiddleware))
  )
}
